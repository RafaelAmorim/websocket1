#!/bin/sh
# THIS SCRIPT IS INTENDED TO BE RUN FROM THE websocket1 PROJECT DIRECTORY
docker run --rm -it --network my-net --name chat-example \
    -v $PWD/simple:/mnt/simple \
    -p 80:3000 \
    -w /mnt/simple     bkoehler/feathersjs sh
